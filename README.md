Napravite igru sa kartama koja ima sledeća pravila:

+ 2 do 4 igrača, ali je igra samo jedan igrač/čovek (ostale igrače kontroliše kompjuter)
+ jedan standardni špil od 52 karte
+ jedno deljenje gde svaki igrač dobija po 10 karata
+ čovek uvek igra prvi
+ kompjuter po slučajnom izboru bira karte
+ svaki igrač izbacuje po jednu kartu i najveća karta nosi sve izbačene karte
+ vrednosti karata su, od najmanje do najveće: A=1, 2 do 10 = vrednost broja koji je na karti, J=12, Q=13, K=14
+ odnešene karte se ne vraćaju u igru
+ kraj igre je kada svi igrači ostanu bez karata u ruci, ne treba dalje deliti preostale karte iz špila
+ pobednik je igrač čiji je zbir vrednosti odnešenih karata najveći (u slučaju istog zbira može biti više pobednika).

+ redosled igranja je u smeru kazaljke na satu
+ ukoliko su dve ili više karata najveće, igrač koji je poslednji bacio tu kartu nosi sve


Tehnički detalji:

+ rešenje napravite korišćenjem React/Redux biblioteke
- zadak treba da ima testove za komponente i za redux
+ rešenje treba biti responsive sa prihvatljivim UI/UX rešenjem (primer prihvatljivog rešenja je na slikama u prilogu)
+ zadatak podići na github, bitbucket ili bilo koji code repository sa public/unauthenticated/anonymous pristupom i poslati link.

+ poželjno je korišćenje ES6+ sintakse
+ koristite https://deckofcardsapi.com/ za deljenje karata, upotreba Piles iz API-ja je opciona

