import React from 'react';
import ReactDOM from 'react-dom';
import GameBoard from './components/container/GameBoard';

import { Provider } from "react-redux";

import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import gameReducer from './reducers/index';

const store = createStore(gameReducer, applyMiddleware(thunk));

ReactDOM.render(
	<div>
		<Provider store={store}>
			<GameBoard />
		</Provider>
	</div>,
	document.getElementById('app')
);