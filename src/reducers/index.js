import { GAME_START, GAME_END, DEAL_CARDS, FETCH_CARDS, initialState, IObject, AiObject } from '../constants';
import { CardValues } from '../constants';

// Create new players array
const createPlayers = (playersCount) => {
	return Array(playersCount).fill(0).map((player, i) =>  {
		return (i === 0) ?
			Object.assign({}, IObject)
			: Object.assign({}, AiObject, { name: AiObject.name + i, id: i });
	});
};

const gameReducer = (state = initialState, action) => {

	switch (action.type) {
		case 'START_NEW_GAME':
			return Object.assign({}, state, action.initialState);

		case GAME_START:
			let players = createPlayers(action.playersCount);

			return Object.assign({}, state, {
				gameStarted: true, 
				players
			});

		case FETCH_CARDS:
			return Object.assign({}, state, {
				cards: action.cards,
				cardsFetched: true
			});

		case DEAL_CARDS:
			let newPlayers = action.players.slice();
			newPlayers[action.i].pile = action.cards.cards;

			return Object.assign({}, state, {
				cardsFetched: true,
				players: newPlayers
			});

		case 'PLAY_TURN':
			let turnPlayers = state.players.slice(),
				talon = [],
				turn = state.turn,
				gameFinished = state.gameFinished;

			// get/throw each players card, starting from player controled user
			turnPlayers.forEach((player, i) => {
				// player throws his/hers card here
				const index = (player.control === 'ai') ? Math.floor(Math.random() * player.pile.length) : action.e;
				const card = player.pile.splice(index, 1)[0];
				talon[i] = card;
			});
			// compare talon card values - calculate turn winner card
			const winnerCard = talon.reduce((prev, current) => (CardValues[prev.value] > CardValues[current.value]) ? prev : current);

			// get turn winner based on card talon index
			const winnerIndex = talon.indexOf(winnerCard);
			let winner = turnPlayers[winnerIndex];

			// reducer function to get the cards sum and assign to leader score
			const { value: sum } = talon.reduce((prev, curr) => {
				prev.value += +CardValues[curr.value];
				return prev;
			}, { value: 0 });
			winner.score += sum;

			// advance turn counter
			turn++;

			// check for game end and calculate the winner
			if (turn === 10) {
				gameFinished = true;
				winner = turnPlayers.reduce((prev, current) => (prev.score > current.score) ? prev : current);
			}

			return Object.assign({}, state, {
				turn,
				talon,
				players: turnPlayers,
				gameFinished,
				winner
			});

		case GAME_END:
			return {
				state: action.state
			};

		default:
			return state;
	}
};

export default gameReducer;