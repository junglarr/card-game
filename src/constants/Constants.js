export const initialState = {
	gameStarted: false,
	players: [],
	turn: 0,
	talon: [],
	error: null,
	gameFinished: false,
	winner: {},
	cardsFetched: false
};


export const CardValues = {
    ACE: 1,
    2: 2,
    3: 3,
    4: 4,
    5: 5,
    6: 6,
    7: 7,
    8: 8,
    9: 9,
    10: 10,
    JACK: 12,
    QUEEN: 13,
    KING: 14
};

/* 
export const UserIdMap = [0, 1, 2, 3];

export const DeckPositions = {
	2: ['bottom', '', 'top', ''],
	3: ['bottom', 'left', '', 'right'],
	4: ['bottom', 'right', 'top', 'left']
};
 */

 export const IObject = {
	name: 'You',
	control: 'i',
	score: 0,
	pile: [],
	id: 0
};

export const AiObject = {
	name: 'Player ',
	control: 'ai',
	score: 0,
	pile: [],
	id: null
};

export const Labels = {
	loading: 'Loading...',
	connectionError: 'There was an error! Please try again.',
	newGame: 'Start new Game',
	backToMenu: 'Back to Menu',
	theHandGoesTo: 'The hand goes to',
	score: 'Score',
	won: 'won',
	gameFinished: 'Game finished'
};