export const GAME_START = 'GAME_START';
export const NEXT_MOVE = 'NEXT_MOVE';
export const GAME_END = 'GAME_END';
export const DEAL_CARDS = 'DEAL_CARDS';
export const FETCH_CARDS = 'FETCH_CARDS';