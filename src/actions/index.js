import ActionTypes from '../constants/ActionTypes';
import axios from 'axios';
import { FETCH_CARDS, DEAL_CARDS, initialState } from '../constants';


export const startNewGame = () => {
	return {
		type: 'START_NEW_GAME',
		initialState
	};
};

export const gameStart = (playersCount) => {
	return {
		type: 'GAME_START',
		playersCount
	};
};

export const fetchThenDeal = (players) => {
	return (dispatch) => {
		const apiUrl = 'https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1';
		axios.get(apiUrl)
			.then(response => {

				const drawUrl = 'https://deckofcardsapi.com/api/deck/' + response.data.deck_id + '/draw/?count=10';
				dispatch({ type: FETCH_CARDS, cards: response.data });

				return players.map( (player, i) => {
					axios.get(drawUrl)
						.then(response => {
							dispatch(fetchRespData(response.data, players, i, DEAL_CARDS));
						});
				});
			})
			.catch(error => {
				throw(error);
			});
	};
};

/* 
export const fetchDeck = () => {
	return (dispatch) => {
		const apiUrl = 'https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1';
		return axios.get(apiUrl)
			.then(response => {
				dispatch(fetchRespData(response.data, players));
			})
			.catch(error => {
				throw(error);
			});
	};
};
 */

 export const fetchRespData = (cards, players, i = 0, type = FETCH_CARDS) => {
	return {
		type: type,
		players,
		i,
		cards
	};
};

export const playTurn = (e) => {
	return {
		type: 'PLAY_TURN',
		e: e.target.id
	};
};