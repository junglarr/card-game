import styled from 'styled-components';

/* Globals start */

export const Clearer = styled.div`
	&:before {
		clear: both;
		content: '';
		display: table;
	}
`;

/* Globals end */
/* Game start  */

export const Container = styled.div`
	position: relative;
	height: 100vh;
`;

export const Center = styled.div`
	position: absolute;
	top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    width: 50%;
    height: 400px;
	margin: auto;
	text-align: center;
`;

/* Game end */
/* GameBoard start */

// grid-template-columns: repeat(7, 2fr 3fr 2fr);
// grid-template-areas:
// "a b c"
// "e f g"
// "h i j";


export const BtnContainer = styled.section`
	margin: 0 auto;
	width: 500px;
	text-align: center;
`;

export const Title = styled.h2`
	xpadding: 50px 20px;
`;

export const StartBtn = styled.div`
	width: 300px;
	margin: 5px auto;
	padding: 15px;
	background: orange; 
	border-radius: 6px;
	text-align: center;
	cursor: pointer;
`;

/* GameBoard end */
/* CardsLoading start */

export const Loader = styled.div`
	width: 340px;
	margin: 0 auto;
	text-align: center;
`;

/* CardsLoader end */
/* ConnectionError start */

export const ConnError = styled.div`
	width: 340px;
	margin: 0 auto;
	text-align: center;
`;

/* ConnectionError end */
/* GameFinished start */

export const FinishedContainer = styled.div`
	margin: 0 auto;
	text-align: center;
`;

export const FinishedCont = styled.div`
	padding-bottom: 25px;
`;

export const FinishedName = styled.span`
	display: inline-block;
	padding: 0 10px 10px 0;
	width: 180px;
	text-align: center;
`;
export const FinishedScore = styled.span`
	display: inline-block;
	padding: 5px 0 0 10px;
	width: 180px;
	text-align: center;
`;

export const Winner = styled.div`
	font-size: 34px;
	padding-bottom: 50px;
	font-weight: bold;
`;

/* GameFinished end */
/* MyCards start */

export const CardsContainer = styled.div`
	width: ${ props => (props.width > 100 ) ? props.width + 0 + 'px' : '100px' };
	margin: 0 auto;
	text-align: center;
`;

export const PlayerInfo = styled.div`
	padding: 0px 0 5px 0;
	justify-content: center;
	display: grid;
`;
export const PlayerName = styled.div`
	width: 150px;
	padding-top: 4px;
	background: orange;
	border-radius: 4px;
	text-align: center;
	font-size: 18px;
`;
export const PlayerScore = styled.div`
	xfloat: right;
	width: 150px;
	padding-bottom: 4px;
	background: orange;
	border-radius: 4px;
	text-align: center;
	font-size: 14px;
`;

/* MyCards end */
/* Card start */

export const CardDiv = styled.div`
	float: left;
	margin-right: -70px;

	@media (max-width: 1200px) {
		margin-right: -70px;
	}
`;
export const CardImg = styled.img`
	width: 90px;
	display: inline-block;
	cursor: pointer;
	border: solid 2px transparent;
	border-radius: 8px;

	&:hover {
		border: solid 2px black;
	}
`;

/* Card end */
/* AiCards start */

export const AiCardsContainer = styled.div`
	margin: 0 auto;
	text-align: center;
	max-width: 260px;

	@media (max-width: 1200px) {
		width: 80px;
	}
`;

/* AiCards end */
/* AiCard start */

export const AiCardDiv = styled.div`
	float: left;
	margin-right: -60px;

	@media (max-width: 1200px) {
		margin-bottom: -130px;
	}
	@media (max-width: 599px) {

	}
`;
export const AiCardImg = styled.img`
	width: 80px;
	display: inline-block;
`;

/* AiCard end */
/* LeaderView start */

export const TurnLeaderContainer = styled.div`
	width: 370px;
	margin: 0 auto;
	text-align: center;
`;

/* LeaderView end */
/* PileView start */

export const PileContainer = styled.section`
	width: ${ props => ( props.width > 100 ) ? props.width + 'px' : '100px' };
	min-width: 130px;
	margin: 20px auto 0 auto;
	text-align: center;
`;

export const PileCard = styled.div`
	display: inline-block;
	padding: 10px 5px;
	@media (max-width: 1200px) {
		padding: 5px;
	}
`;

export const PileCardContainer = styled.div`
	border: ${ props => ( props.isWinner ) ? 'solid 2px orange' : 'none' };
	background: ${ props => ( props.isWinner ) ? 'orange' : 'inherit' };
	border-radius: ${ props => ( props.isWinner ) ? '6px' : '0px' };
	color: ${ props => ( props.isWinner ) ? 'white' : '' };
	float: left;
`;

/* PileView end */
/* Layout start */

export const MainContainer = styled.div`
	display: grid;
	grid-template-columns: auto 2fr auto;
	height: 100vh;
`;
	export const ColLeft = styled.section`
		display: grid;
		justify-self: start;
		display: grid;
		align-items: center;
	`;
	export const ColMid = styled.section`
		display: grid;
		grid-template-rows: 2fr 4fr 2fr;
		justify-self: center;
	`;
	export const ColRight = styled.section`
		display: grid;
		justify-self: end;
		display: grid;
		align-items: center;
	`;
export const Blank = styled.div`
`;
export const Pile = styled.div`
	display: grid;
	align-items: center;
`;
export const Deck = styled.div`
	//transform: ${ props => 'rotate(-'+ props.indexRot * 90 +'deg)' };
	align-self: center;
  	justify-self: center;
	//padding: 10px;
	min-height: 180px;
	min-width: 300px;
	@media (max-width: 1200px) {
		min-width: 240px;
	}
`;



export const TableRows = styled.section`
	display: grid;
	grid-template-rows: 1fr 1fr 1fr;
	height: 100vh;
`;
export const RowTop = styled.div`
	display: grid;
	grid-template-columns: auto 370px auto;
	justify-items: center;
`;

export const RowMiddle = styled.div`
	display: grid;
	grid-template-columns: minmax(100px, auto) max-content minmax(100px, auto);
	justify-items: normal;
`;

export const RowBottom = styled.div`
	display: grid;
	grid-templatecolumns: auto 370px auto;
	justify-items: center;
`;

/* Layout end */