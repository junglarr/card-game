import React, { Component } from 'react';
import GameFinished from '../display/GameFinished';
import FourPlayerBoard from '../display/FourPlayerBoard';
import ThreePlayerBoard from '../display/ThreePlayerBoard';
import TwoPlayerBoard from '../display/TwoPlayerBoard';
import { Container } from '../../styles/all-styles';
// import ConnectionError from '../display/ConnectionError';

import { fetchThenDeal, playTurn, startNewGame } from '../../actions/index';

import { connect } from 'react-redux';

class GameView extends Component {

	componentDidMount() {
		this.props.fetchThenDeal(this.props.players);
	}

	render() {
		const { players, winner, gameFinished, startNewGame } = this.props;

		if (gameFinished) {
			return(
				<>
					<GameFinished players={ players } winner={ winner } newGame={ startNewGame } />
				</>
			);
		}
		else {
			return (
				<Container>
					{ (players.length === 4) ?
						<FourPlayerBoard {...this.props} />
					: (players.length === 3) ?
						<ThreePlayerBoard {...this.props} />
					:
						<TwoPlayerBoard {...this.props} />
					}
				</Container>
			);
		}
	}
};

const mapStateToProps = state => {
	return {
		players: state.players,
		talon: state.talon,
		winner: state.winner,
		cardsFetched: state.cardsFetched,
		gameFinished: state.gameFinished
	};
};

const mapDispatchToProps = {
	fetchThenDeal,
	playTurn,
	startNewGame
};
// GameView.defaultProps = {
// 	players: [], talon: [], winner: [], cardsFetched: bool, fetchThenDeal: function, playTurn: function
// };

export default connect(mapStateToProps, mapDispatchToProps)(GameView);