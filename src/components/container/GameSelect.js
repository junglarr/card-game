import React from "react";
import { gameStart } from '../../actions/index';
import { BtnContainer, Title, StartBtn, Container, Center } from '../../styles/all-styles';

import { connect } from "react-redux";

const GameSelect = ({ gameStart }) => {
	return (
		<Container>
			<Center>
				<BtnContainer>
					<Title>Choose your game</Title>
					<StartBtn onClick={ () => gameStart(2) }>2 Player game</StartBtn>
					<StartBtn onClick={ () => gameStart(3) }>3 Player game</StartBtn>
					<StartBtn onClick={ () => gameStart(4) }>4 Player game</StartBtn>
				</BtnContainer>
			</Center>
		</Container>
	);
};

const mapDispatchToProps = {
	gameStart
};

const GameSelectContainer = connect(null, mapDispatchToProps)(GameSelect);
export default GameSelectContainer;