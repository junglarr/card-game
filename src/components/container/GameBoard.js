import React from 'react';
import GameSelect from './GameSelect';
import GameView from './GameView';

import { connect } from 'react-redux';

const GameBoard = ({ gameStarted }) => {
	return (
		<>
			{ (!gameStarted) ? 
				<GameSelect />
			:
				<GameView />
			}
		</>
	);
};

const mapStateToProps = state => {
	return {
		gameStarted: state.gameStarted
	};
};

const GameBoardContainer = connect(mapStateToProps)(GameBoard);
export default GameBoardContainer;