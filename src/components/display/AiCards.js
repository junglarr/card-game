import React from 'react';
import AiCard from './AiCard';
import { AiCardsContainer, Clearer, PlayerInfo, PlayerName, PlayerScore } from '../../styles/all-styles';

const AiCards = props => {

	return (
		<>
			<PlayerInfo>
				<PlayerName>{ props.player.name }</PlayerName>
				<PlayerScore>{ props.labels.score }: { props.player.score }</PlayerScore>
				<Clearer></Clearer>
			</PlayerInfo>
			<AiCardsContainer>
				{ (props.player.pile) ? 
					props.player.pile.map((card, i) => {
						return (
							<AiCard card={ card } key={ i } />
						);
					} )
				: '' }
				<Clearer></Clearer>
			</AiCardsContainer>
		</>
	);
};

export default AiCards;