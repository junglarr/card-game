import React from 'react';
import { CardDiv, CardImg } from '../../styles/all-styles';

const CardWrapper = ({ onClick, cardKey, card }) => {
	return (
		<div>
			<CardDiv key={ cardKey }>
				<CardImg id={ cardKey } onClick={ onClick } src={ card.image } />
			</CardDiv>
		</div>
	);
};

const Card = props => {
	return <CardWrapper onClick={ (e) => props.playTurn(e) } cardKey={ props.cardKey } card={ props.card }>Some Children</CardWrapper>;
};

export default Card;