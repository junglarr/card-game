import React from 'react';
import { AiCardDiv, AiCardImg } from '../../styles/all-styles';

const AiCard = () => {
	return (
		<AiCardDiv>
			<AiCardImg src='img/hs_cb.jpg' />
		</AiCardDiv>
	);
}

export default AiCard;