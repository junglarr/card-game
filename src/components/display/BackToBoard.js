import React from 'react';
import { StartBtn } from '../../styles/all-styles';

const BackToBoard = props => {
	return (
		<StartBtn id={ 0 } onClick={(e) => props.newGame(e)}>
			{ props.labels.backToMenu }
		</StartBtn>
	);
}

export default BackToBoard;