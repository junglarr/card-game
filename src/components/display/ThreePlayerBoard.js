import React from 'react';
import { Labels } from '../../constants';
import CardsLoading from './CardsLoading';
import MyCards from './MyCards';
import AiCards from './AiCards';
import PileView from './PileView';
import { TableRows, RowTop, RowMiddle, RowBottom } from '../../styles/all-styles';

const ThreePlayerBoard = props => {
	const iPlayer = props.players.filter(player => player.control !== 'ai')[0];

	return (
		<TableRows>
			<RowTop style={{ display: 'grid', gridTemplateColumns: 'auto 370px auto', justifyItems: 'center' }}>
				<div></div>
				<div></div>
				<div></div>
			</RowTop>
			<RowMiddle style={{ display: 'grid', gridTemplateColumns: 'minmax(100px, auto) minmax(370px, calc(100% - 220px)) minmax(100px, auto)', justifyItems: 'center' }}>
				<div>
					{ ( props.players[1] )
						? <AiCards player={ props.players[1] } labels={ Labels } cardsFetched={ props.cardsFetched } />
					: '' }
				</div>
				<div>
					<PileView talon={ props.talon } players={ props.players } winner={ props.winner } labels={ Labels }/>
				</div>
				<div>
					{ ( props.players[2] )
						? <AiCards player={ props.players[2] } labels={ Labels } cardsFetched={ props.cardsFetched } />
					: '' }
				</div>
			</RowMiddle>
			<RowBottom style={{ display: 'grid', gridTemplateColumns: 'auto 370px auto', justifyItems: 'center' }}>
				<div></div>
				<div>
					<MyCards player={ iPlayer } labels={ Labels } playTurn={ props.playTurn } cardsFetched={ props.cardsFetched } />
				</div>
				<div></div>
			</RowBottom>
		</TableRows>
	);
}

export default ThreePlayerBoard