import React from 'react';
import { ConnError } from '../../styles/all-styles';

const ConnectionError = () => {
	return (
		<ConnError>{ props.labels.connectionError }</ConnError>
	);
}

export default ConnectionError;