import React from 'react';
import { Labels } from '../../constants';
import CardsLoading from './CardsLoading';
import MyCards from './MyCards';
import AiCards from './AiCards';
import PileView from './PileView';
import { TableRows, RowTop, RowMiddle, RowBottom } from '../../styles/all-styles';

const TwoPlayerBoard = props => {
	const iPlayer = props.players.filter(player => player.control !== 'ai')[0];

	return (
		<TableRows>
			<RowTop>
				<div></div>
				<div>
					{ ( props.players[1] ) ?
						<AiCards player={ props.players[1] } labels={ Labels } cardsFetched={ props.cardsFetched } />
					: '' }
				</div>
				<div></div>
			</RowTop>
			<RowMiddle>
				<div>
				</div>
				<div>
					<PileView talon={ props.talon } players={ props.players } winner={ props.winner } labels={ Labels }/>
				</div>
				<div>
				</div>
			</RowMiddle>
			<RowBottom>
				<div></div>
				<div>
					<MyCards player={ iPlayer } labels={ Labels } playTurn={ props.playTurn } cardsFetched={ props.cardsFetched } />
				</div>
				<div></div>
			</RowBottom>
		</TableRows>
	);
}

export default TwoPlayerBoard;