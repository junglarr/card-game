import React from 'react';
import { Labels } from '../../constants';
import { Container, Center, Player, Winner, FinishedCont, FinishedName, FinishedScore } from '../../styles/all-styles';
import BackToBoard from './BackToBoard';

const GameFinished = props => {

	return (
		<Container>
			<Center>
				<Winner>{ props.winner.name } { Labels.won }!</Winner>
				<FinishedCont>
				{ props.players.map((player, id) => {
					return 	(
							<div key={ id }>
								<FinishedName>{ player.name }</FinishedName><FinishedScore>{ player.score }</FinishedScore>
							</div>
					)
				}) }
				</FinishedCont>
				<BackToBoard labels={ Labels } newGame={ props.newGame } />
			</Center>
		</Container>
	);
}

export default GameFinished;