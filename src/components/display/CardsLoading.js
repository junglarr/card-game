import React from 'react';
import { Loader } from '../../styles/all-styles';

const CardsLoading = () => {
	return (
		<Loader>
			{ props.labels.loading }
		</Loader>
	);
}

export default CardsLoading;