import React from 'react';
import Card from './Card';
import { CardsContainer, Clearer, PlayerInfo, PlayerName, PlayerScore } from '../../styles/all-styles';

const MyCards = props => {

		const width = (props.player.pile.length + 1) * 30 + 10;

		return (
			<>
				<CardsContainer width={ width }>
					{ props.player.pile.map((card, i) => {
						return (
							<Card card={ card } key={ i } cardKey={ i } playTurn={ props.playTurn } />
						);
					} ) }
					<Clearer></Clearer>
				</CardsContainer>
				<PlayerInfo>
					<PlayerName style={{ float: 'left' }}>{ props.player.name }</PlayerName>
					<PlayerScore style={{ float: 'right' }}>{ props.labels.score }: { props.player.score }</PlayerScore>
					<Clearer></Clearer>
				</PlayerInfo>
			</>
		);
};

export default MyCards;