import React from 'react';
import { PileContainer, PileCardContainer, PileCard, Clearer } from "../../styles/all-styles";

const PileView = props => {

	const width = props.talon.length * 120;

	return (
		<PileContainer width={ width }>
			{ props.talon.map( (card, i) => {
				let isWinner = (props.winner.id == props.players[i].id);
				return  (
						<PileCardContainer key={ i } isWinner={ isWinner }>
							<PileCard>
								<img width="100px" src={ card.image } />
							</PileCard>
						</PileCardContainer>
					);
			} ) }
			<Clearer></Clearer>
		</PileContainer>
	);
};

export default PileView;