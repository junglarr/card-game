const path = require('path');
// html webpack plugin - generates index.html page for you, wihch includes necessary scripts
const HtmlWebpackPlugin = require('html-webpack-plugin');
// clean webpack pluign - clean ./dist/ folder upon every build (remove old files)
const CleanWebpackPlugin = require('clean-webpack-plugin');

const config  = {
	'devtool': 'inline-source-map',
	'module': {
		'rules': [
			{
				'test': /\.(js|jsx)$/,
				'use': 'babel-loader',
				'exclude': /node_modules/
			}
		]
	},
	'resolve': {
		'extensions': [ '.js', '.jsx' ]
	},
	'entry': {
		'app': './src/index.js'
	},
	'output': {
		'filename': '[name].bundle.js',
		'path': path.resolve(__dirname, 'dist')
	},
	'plugins': [
		new CleanWebpackPlugin(['dist']),
		new HtmlWebpackPlugin({
			'title': 'Development',
			'filename': 'index.html',
			'template': './index.html',
			'meta': {
				'viewport': 'width=device-width, initial-scale=1, shrink-to-fit=no',
			},
			'inject': 'body'
		})
	]
};

module.exports = config;